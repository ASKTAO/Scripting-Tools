# 使用openpyxl,拆分Excel文档。


### 0、指定需要拆分的Excel路径
    excel_path = Path("C:\\Users\\Administrator\\Desktop\\test.xlsx")

### 1、引入该库
    import MyExcelTool

### 2、实例化MyExcelTool对象
    MyExcel = MyExcelTool()

### 3、加载指定路径的Excel
    MyExcel = loadXL(excel_path)

### 4、使用splitXL方法，拆分Excel表格，并按该
##### splitXL(column=1,title_rows=1,sh_name="Sheet1",)
##### column：代表列数，从1计数。
##### title_rows：标题行数，新生成的Excel使用源文档的标题，指定标题行数。从1计数。
    MyExcel.splitXL(4)

# 执行结果是：
    1、在该文档同级别的目录下，新建与文档同名的文件夹。
    2、并指定列的值为文件名，将该列所有相同的数据，放入该列名的文档中。
